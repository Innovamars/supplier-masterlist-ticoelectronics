Solucionar el tema de:
 import { FormsModule } from '@angular/forms';
 

#comando de instalación
npm install @angular/cli@7.2.2
ng new masterlist
routing yes
css

#dependencias
npm i boostrap -S
npm i bootstrap -S
npm i jquery - S
npm i popper.js -S
npm i ngx-spinner -S

--save ó -S es para agregarlo al paquete json.


npm i firebase -S
npm i @angular/fire -S
npm i font-awesome -S

#componentes 
Se crearon los siguientes componentes:
- com
para agregar los formularios COM utilizados por Masterlist
COM es un termino utilizado por Tico.
    - COM avtres
    - COM ptresespanol
    - COM ptres ingles
    - COM dieciseisingles
    - COM diecisiete
    - COM nueve
- navbar 
menu de navegacion
- page404
pagina de errores: cuando el usuario utiliza una ruta que no existe
-supplier 
datos de los proveedores registrados
    - green
    - new
        - calidad 
        - conta
        - compras
    - red
    - yellow
- user
    - login
    - profile
    - register
- home
- hero

#Service
- auth
-dataApi

#guards
- auth

#routing


#*ngIf="isLogged"
es para definir la verificacion de una variable, por ejemplo si esta logiado

# Masterlist

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
