import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PtresespanolComponent } from './components/com/ptresespanol/ptresespanol.component';
import { AVtresComponent } from './components/com/avtres/avtres.component';
import { PtresinglesComponent } from './components/com/ptresingles/ptresingles.component';
import { RdieciseisespanolComponent } from './components/com/rdieciseisespanol/rdieciseisespanol.component';
import { RdieciseisinglesComponent } from './components/com/rdieciseisingles/rdieciseisingles.component';
import { RnueveComponent } from './components/com/rnueve/rnueve.component';
import { HeroComponent } from './components/hero/hero.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { GreenComponent } from './components/supplier/green/green.component';
import { CalidadComponent } from './components/supplier/new/calidad/calidad.component';
import { ComprasComponent } from './components/supplier/new/compras/compras.component';
import { ContaComponent } from './components/supplier/new/conta/conta.component';
import { RedComponent } from './components/supplier/red/red.component';
import { YellowComponent } from './components/supplier/yellow/yellow.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { Page404Component } from './components/page404/page404.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'COM/AVtres', component: AVtresComponent },
  { path: 'COM/ptresespanolComponent', component: PtresespanolComponent },
  { path: 'COM/ptresingles', component: PtresinglesComponent },
  { path: 'COM/rdieciseisespanol', component: RdieciseisespanolComponent },
  { path: 'COM/rdieciseisingles', component: RdieciseisinglesComponent },
  { path: 'COM/rnueve', component: RnueveComponent },
  { path: 'hero', component: HeroComponent },
  { path: 'navbar', component: NavbarComponent },
  { path: 'supplier/green', component: GreenComponent },
  { path: 'supplier/new/calidad', component: CalidadComponent },
  { path: 'supplier/new/compras', component: ComprasComponent },
  { path: 'supplier/new/conta', component: ContaComponent },
  { path: 'supplier/red', component: RedComponent },
  { path: 'supplier/yellow', component: YellowComponent },
  { path: 'user/login', component: LoginComponent },
  { path: 'user/register', component: RegisterComponent },
  { path: 'user/profile', component: ProfileComponent },
  { path: '**', component: Page404Component }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
