import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/user/login/login.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { RegisterComponent } from './components/user/register/register.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RedComponent } from './components/supplier/red/red.component';
import { GreenComponent } from './components/supplier/green/green.component';
import { YellowComponent } from './components/supplier/yellow/yellow.component';
import { ContaComponent } from './components/supplier/new/conta/conta.component';
import { ComprasComponent } from './components/supplier/new/compras/compras.component';
import { CalidadComponent } from './components/supplier/new/calidad/calidad.component';
import { AVtresComponent } from './components/com/avtres/avtres.component';
import { PtresinglesComponent } from './components/com/ptresingles/ptresingles.component';
import { PtresespanolComponent } from './components/com/ptresespanol/ptresespanol.component';
import { RnueveComponent } from './components/com/rnueve/rnueve.component';
import { RdieciseisespanolComponent } from './components/com/rdieciseisespanol/rdieciseisespanol.component';
import { RdieciseisinglesComponent } from './components/com/rdieciseisingles/rdieciseisingles.component';
import { RdiecisieteComponent } from './components/com/rdiecisiete/rdiecisiete.component';
import { HeroComponent } from './components/hero/hero.component';
import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './components/page404/page404.component';
import { FormsModule } from '@angular/forms';


import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { environment } from 'src/environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    NavbarComponent,
    RedComponent,
    GreenComponent,
    YellowComponent,
    ContaComponent,
    ComprasComponent,
    CalidadComponent,
    AVtresComponent,
    PtresinglesComponent,
    PtresespanolComponent,
    RnueveComponent,
    RdieciseisespanolComponent,
    RdieciseisinglesComponent,
    RdiecisieteComponent,
    Page404Component,
    HeroComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule

  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
