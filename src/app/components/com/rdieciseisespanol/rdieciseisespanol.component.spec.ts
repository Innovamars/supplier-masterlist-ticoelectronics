import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdieciseisespanolComponent } from './rdieciseisespanol.component';

describe('RdieciseisespanolComponent', () => {
  let component: RdieciseisespanolComponent;
  let fixture: ComponentFixture<RdieciseisespanolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdieciseisespanolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdieciseisespanolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
