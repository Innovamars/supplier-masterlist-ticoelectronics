import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RnueveComponent } from './rnueve.component';

describe('RnueveComponent', () => {
  let component: RnueveComponent;
  let fixture: ComponentFixture<RnueveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RnueveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RnueveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
