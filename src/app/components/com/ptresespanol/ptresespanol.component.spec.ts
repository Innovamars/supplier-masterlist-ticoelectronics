import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtresespanolComponent } from './ptresespanol.component';

describe('PtresespanolComponent', () => {
  let component: PtresespanolComponent;
  let fixture: ComponentFixture<PtresespanolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtresespanolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtresespanolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
