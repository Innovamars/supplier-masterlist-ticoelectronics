import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdieciseisinglesComponent } from './rdieciseisingles.component';

describe('RdieciseisinglesComponent', () => {
  let component: RdieciseisinglesComponent;
  let fixture: ComponentFixture<RdieciseisinglesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdieciseisinglesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdieciseisinglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
