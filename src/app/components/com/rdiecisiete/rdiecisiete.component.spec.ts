import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdiecisieteComponent } from './rdiecisiete.component';

describe('RdiecisieteComponent', () => {
  let component: RdiecisieteComponent;
  let fixture: ComponentFixture<RdiecisieteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdiecisieteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdiecisieteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
