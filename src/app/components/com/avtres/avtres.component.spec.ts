import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AVtresComponent } from './avtres.component';

describe('AVtresComponent', () => {
  let component: AVtresComponent;
  let fixture: ComponentFixture<AVtresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AVtresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AVtresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
