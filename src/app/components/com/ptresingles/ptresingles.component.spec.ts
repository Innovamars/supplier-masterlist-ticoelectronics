import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PtresinglesComponent } from './ptresingles.component';

describe('PtresinglesComponent', () => {
  let component: PtresinglesComponent;
  let fixture: ComponentFixture<PtresinglesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PtresinglesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PtresinglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
